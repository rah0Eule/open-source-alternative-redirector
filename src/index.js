// ==UserScript==
// @name Open-Source Alternative Redirector
// @name:ar معيد التوجيه البديل مفتوح المصدر
// @name:bg Алтернативно пренасочване с отворен код
// @name:cs Open-Source alternativní přesměrovač
// @name:da Open Source Alternativ Redirector
// @name:de Open-Source-alternativer Redirector
// @name:es Redirector alternativo de código abierto
// @name:fi Open-Source Alternative Redirector
// @name:fr Redirecteur alternatif open source
// @name:he מפנה אלטרנטיבי בקוד פתוח
// @name:it Reindirizzamento alternativo open source
// @name:ja オープンソースの代替リダイレクター
// @name:ko 오픈 소스 대체 리디렉터
// @name:nl Alternatieve Open Source-redirector
// @name:pl Alternatywny readresator typu open source
// @name:ro Redirector alternativ cu sursă deschisă
// @name:ru Альтернативный перенаправитель с открытым исходным кодом
// @name:tr Açık Kaynak Alternatif Yönlendirici
// @name:uk Альтернативний перенаправник з відкритим вихідним кодом
// @name:zh-CN 开源替代重定向器
// @name:zh-TW 開源替代重定向器
// @namespace -
// @version 9.0.1
// @description Redirects you from proprietary web-services to ethical alternatives(front-end).
// @description:ar يعيد توجيهك من خدمات الويب المسجلة الملكية إلى البدائل الأخلاقية (الواجهة الأمامية).
// @description:bg Пренасочва ви от собствени уеб-услуги към етични алтернативи (front-end).
// @description:cs Přesměruje vás z proprietárních webových služeb na etické alternativy (front-end).
// @description:da Omdirigerer dig fra proprietære web-tjenester til etiske alternativer (front-end).
// @description:de Leitet Sie von proprietären Webdiensten zu ethischen Alternativen (Front-End) weiter.
// @description:es Lo redirige de servicios web propietarios a alternativas éticas (front-end).
// @description:fi Ohjaa sinut patentoiduista verkkopalveluista eettisiin vaihtoehtoihin (käyttöliittymä).
// @description:fr Vous redirige des services Web propriétaires vers des alternatives éthiques (front-end).
// @description:he מפנה אותך משירותי אינטרנט קנייניים לחלופות אתיות (חזית).
// @description:it Ti reindirizza da servizi web proprietari ad alternative etiche (front-end).
// @description:ja 独自のWebサービスから倫理的な代替手段（フロントエンド）にリダイレクトします。
// @description:ko 독점 웹 서비스에서 윤리적 대안(프론트 엔드)으로 리디렉션합니다.
// @description:nl Leidt u om van propriëtaire webservices naar ethische alternatieven (front-end).
// @description:pl Przekierowuje Cię z zastrzeżonych usług internetowych do etycznych alternatyw (front-end).
// @description:ro Vă redirecționează de la servicii web proprietare la alternative etice (front-end).
// @description:ru Перенаправляет вас с проприетарных веб-сервисов на этические альтернативы (интерфейс).
// @description:tr Sizi tescilli web hizmetlerinden etik alternatiflere (ön uç) yönlendirir.
// @description:uk Перенаправляє вас із власних веб-сервісів до етичних альтернатив (фронт-енд).
// @description:zh-CN 将您从专有网络服务重定向到道德替代品（前端）。
// @description:zh-TW 將您從專有網絡服務重定向到道德替代品（前端）。
// @author NotYou
// @include *youtube.com/*
// @include *bing.com/*
// @include *reddit.com/*
// @include *twitter.com/*
// @run-at document-start
// @license GPL-3.0-or-later
// @icon https://icons.iconarchive.com/icons/itweek/knob-toolbar/32/Knob-Shuffle-Off-icon.png
// @grant none
// ==/UserScript==

var url = new URL(location.href),

// INSTANCES //
invidious = 'invidious.kavin.rocks',
ddg = 'duckduckgo.com',
libreddit = 'libreddit.spike.codes',
nitter = 'nitter.net'

// YouTube | Invidious //
if(location.host.indexOf('youtube.com') != -1){
    location.replace('https://' + invidious + location.pathname + location.search)
}

// Bing | DuckDuckGo //
if(location.host.indexOf('bing.com') != -1){
    location.replace('https://' + ddg + location.search)
}

// Reddit | Libreddit //
if(location.host.indexOf('reddit.com') != -1){
    location.replace('https://' + libreddit + location.pathname + location.search)
}

// Twitter | Nitter //
if(location.host.indexOf('twitter.com') != -1){
    location.replace('https://' + nitter + location.pathname + location.search)
}

